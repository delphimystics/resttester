unit frmMain;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  REST.Types, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  System.Rtti, FMX.Grid.Style, Data.Bind.EngExt, Fmx.Bind.DBEngExt,
  Fmx.Bind.Grid, System.Bindings.Outputs, Fmx.Bind.Editors,
  Data.Bind.Components, Data.Bind.Grid, Data.Bind.DBScope,
  FMX.Controls.Presentation, FMX.ScrollBox, FMX.Grid, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, REST.Response.Adapter,
  REST.Client, Data.Bind.ObjectScope, FMX.StdCtrls;

type
  TformMain = class(TForm)
    RESTClient1: TRESTClient;
    RESTRequest1: TRESTRequest;
    RESTResponse1: TRESTResponse;
    RESTResponseDataSetAdapter1: TRESTResponseDataSetAdapter;
    FDMemTable1: TFDMemTable;
    Grid1: TGrid;
    BindSourceDB1: TBindSourceDB;
    BindingsList1: TBindingsList;
    LinkGridToDataSourceBindSourceDB1: TLinkGridToDataSource;
    btnTestGetHTTP: TButton;
    lblTitle: TLabel;
    aniLoading: TAniIndicator;
    btnTestGetHTTPS: TButton;
    btnTestPostHTTP: TButton;
    btnTestPostHTTPS: TButton;
    procedure btnTestGetHTTPClick(Sender: TObject);
    procedure btnTestGetHTTPSClick(Sender: TObject);
    procedure btnTestPostHTTPClick(Sender: TObject);
    procedure btnTestPostHTTPSClick(Sender: TObject);
  private
    { Private declarations }
    procedure RestSuccess(Protocol: String; Mode: String);
    procedure RestFailure(Protocol: String; Mode: String);
  public
    { Public declarations }
  end;

var
  formMain: TformMain;

implementation

{$R *.fmx}

procedure TformMain.RestSuccess(Protocol: String; Mode: String);
begin
  TThread.Synchronize(nil,
    procedure
    begin
      lblTitle.Text := 'REST Test - Win32 - Release - FMX - Test Complete ' + Protocol + ' - ' + Mode + ' - Test Success ' + FormatDateTime('mm/dd/yyyy hh:nn:sss.zzzam/pm',Now);
      aniLoading.Enabled := False;
    end);
end;

procedure TformMain.btnTestGetHTTPClick(Sender: TObject);
begin
  aniLoading.Enabled := True;
  lblTitle.Text := 'REST Test - Win32 - Release - FMX - Testing...';
  RESTClient1.BaseURL := 'http://reqres.in';
  RESTRequest1.Resource := '/api/users';
  RESTRequest1.Method := rmGET;
  RESTRequest1.Params.AddItem('page', '2', TRESTRequestParameterKind.pkGETorPOST, [], TRESTContentType.ctNone);
  RESTRequest1.ExecuteAsync(
    procedure
      begin
        RestSuccess('HTTP', 'Get');
      end,
    true,
    false,
    procedure(sender: TObject)
      begin
        RestFailure('HTTP', 'Get');
      end);
end;

procedure TformMain.btnTestGetHTTPSClick(Sender: TObject);
begin
  aniLoading.Enabled := True;
  lblTitle.Text := 'REST Test - Win32 - Release - FMX - Testing...';
  RESTClient1.BaseURL := 'https://reqres.in';
  RESTRequest1.Resource := '/api/users';
  RESTRequest1.Method := rmGet;
  RESTRequest1.Params.Clear;
  RESTRequest1.Params.AddItem('page', '2', TRESTRequestParameterKind.pkGETorPOST, [], TRESTContentType.ctNone);
  RESTRequest1.ExecuteAsync(
    procedure
      begin
        RestSuccess('HTTPS', 'Get');
      end,
    true,
    true,
    procedure(sender: TObject)
      begin
        RestFailure('HTTPS', 'Get');
      end);
end;

procedure TformMain.btnTestPostHTTPClick(Sender: TObject);
begin
  aniLoading.Enabled := True;
  lblTitle.Text := 'REST Test - Win32 - Release - FMX - Testing...';
  RESTClient1.BaseURL := 'http://reqres.in';
  RESTRequest1.Resource := '/api/login';
  RESTRequest1.Method := rmPOST;
  RESTRequest1.Params.Clear;
  RESTRequest1.Params.AddBody('{"email": "eve.holt@reqres.in","password": "cityslicka"}',TRESTContentType.ctNone);
  RESTRequest1.ExecuteAsync(
    procedure
      begin
        RestSuccess('HTTP', 'Post');
      end,
    true,
    true,
    procedure(sender: TObject)
      begin
        RestFailure('HTTP', 'Post');
      end);
end;

procedure TformMain.btnTestPostHTTPSClick(Sender: TObject);
begin
  aniLoading.Enabled := True;
  lblTitle.Text := 'REST Test - Win32 - Release - FMX - Testing...';
  RESTClient1.BaseURL := 'https://reqres.in';
  RESTRequest1.Resource := '/api/login';
  RESTRequest1.Method := rmPOST;
  RESTRequest1.Params.Clear;
  RESTRequest1.Params.AddBody('{"email": "eve.holt@reqres.in","password": "cityslicka"}',TRESTContentType.ctNone);
  RESTRequest1.ExecuteAsync(
    procedure
      begin
        RestSuccess('HTTPS', 'Post');
      end,
    true,
    true,
    procedure(sender: TObject)
      begin
        RestFailure('HTTPS', 'Post');
      end);
end;

procedure TformMain.RestFailure(Protocol: String; Mode: String);
begin
  TThread.Synchronize(nil,
    procedure
    begin
      lblTitle.Text := 'REST Test - Win32 - Release - FMX - ' + Protocol + ' - ' + Mode + ' - Test Failed ' + FormatDateTime('mm/dd/yyyy hh:nn:sss.zzzam/pm',Now);
      aniLoading.Enabled := False;
    end);
end;

end.
